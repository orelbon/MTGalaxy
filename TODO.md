Battle Simulator release:
1. Debug functions for scene-loading / etc - should be easy to chat. Must be blocked behind host player.
2. Battler has to be done, waiting for animations before continuing so we can test animations.
3. Item system has to be coded in, figure out a way to store items per player. (UI, physical item?)
4. Polish the existing systems and ensure no bugs
5. Finish saving and loading systems
6. Finish making the battle sim map
7. Bugtesting

Battler
- [ ] Complete Battler Module 
  - [ ] Create basic animations (Pokemon, Pokeball) for animation testing
