## Pokemon Master Trainer Galaxy

### Folder Structure

* **Main** *This is the main folder where scripts are stored*
  * **Data** *Data for various tables are stored here*
  * **global_files** *These files need to be placed in the Global.lua file*
  * **object_scripts** *These files are placed on individual objects spawned using the scenemanager.*
* **Pokedex** *Scripts for the Pokedex (used by all modules) is stored here.*

### Object scripts vs. Global
In this project we use object scripts vs. global ones, this is so that the objects are considered modular and can be easily copy-pasted to other saves if players want to include them.